package com.kenfogel.jspservletexample01.business;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import com.kenfogel.jspservletexample01.bean.User;

public class UserIO {

    public synchronized void addRecord(User user, String filename) throws IOException {
        File file = new File(filename);
        try (PrintWriter out = new PrintWriter(
                new FileWriter(file, true))) {
            out.println(user.getEmail() + "|"
                    + user.getFirstName() + "|"
                    + user.getLastName());
        }
    }
}
